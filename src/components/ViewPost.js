import { POST, USER, LOADER } from '../application/selector';
import { useSelector,useDispatch } from 'react-redux';
import { useEffect, useState } from 'react';
import { getAllPost } from '../application/post.redux/post.actions';
import { useParams } from "react-router-dom";
import {Container} from '@mui/material';
import { getUser } from '../application/user.redux/user.action';
import LoadingSpinner from './Spinner';
import { CardDetailView } from './CustomCard';
import { ViewDialog } from './CustomDialog';


const ViewPost = () => {
    const dispatch=useDispatch();
    const {id}=useParams();
    const data=useSelector(POST);
    const userDetail=useSelector(USER);
    const isLoading = useSelector(LOADER);
    const  [postDetail, setPostDetail] = useState({});
    const [open, setOpen] = useState(false);
    //console.log(isLoading)
    const handleClickOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
    };

    useEffect(()=>{
        if(data?.length===0)
        {            
            dispatch(getAllPost());
        }              
        
        if(data?.length > 0)
        {
            const detail = data?.find(e=>e.id === Number(id));
            if(detail?.userId) 
            {
                dispatch(getUser(detail.userId));
            }
            if(detail)
            {
                const obj = {           
                    title : detail.title,
                    body: detail.body,
                    userId : detail.userId,
                    tags : detail.tags,
                    reactions : detail.reactions
                }
                setPostDetail(obj);   
            }  
        }
        
    },[data]); 
 

    return(
        <Container maxWidth="md">
            {
                isLoading ? (<LoadingSpinner /> ) : (
                (postDetail?.title && postDetail?.body && postDetail?.userId) ? (
                    <CardDetailView handleClickOpen={handleClickOpen} postDetail={postDetail} userDetail={userDetail} />
                ) : (
                    <div className='post-card'><p className='home-context'>Oops! Not found</p></div>
                ))
            }                
            <a className='post-link' href='/posts'>Go back...</a>
            <ViewDialog open={open} handleClose={handleClose} userDetail={userDetail} />
        </Container>
    )
}

export default ViewPost;
