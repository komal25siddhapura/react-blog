import {Container,Stack, Pagination, Grid} from '@mui/material';
import './Posts.css';
import { useEffect, useState } from 'react';
import usePagination from '../Utils/pagination'
import { useSelector,useDispatch } from 'react-redux';
import {POST, LOADER} from '../application/selector';
import { getAllPost} from '../application/post.redux/post.actions';
import LoadingSpinner from './Spinner';
import { CustomCard } from './CustomCard';
import { UpdatePageNo, getPageNo } from '../Utils/localStorage';

const Posts=() => {
    const dispatch=useDispatch();
    const [page, setPage] = useState(1);
    const PER_PAGE = 5;
    const data=useSelector(POST);
    const isLoading= useSelector(LOADER);    
    const count = Math.ceil(data?.length / PER_PAGE);
    const _DATA = usePagination(data, PER_PAGE);

    useEffect(()=>{
        
        if(data?.length===0)
        {
            dispatch(getAllPost());
        } 
        setPage(Number(getPageNo()));
    },[])
 
    const handleOnChange = (e, p) => {  
        UpdatePageNo(p);
        setPage(p);
        _DATA.jump(p);
    }
    return(
        <div>
            <Container  maxWidth="lg"> 
                <div className='post-header'>
                    <h1> The ocean of posts </h1>
                </div>                    
                    {isLoading? (<LoadingSpinner />) : (
                        <>       
                            <Grid container>                    
                                <Grid item xs={12}>
                                    <div>                    
                                        {_DATA?.currentData().map(item => {
                                            const {title, body, tags, reactions,id}=item;                        
                                            return (
                                                <div key={id}>
                                                    {
                                                        (title && body) ? <CustomCard title={title} body={body} tags={tags} reactions={reactions} id={id}/> : <></>                
                                                    }
                                                </div>   
                                            );                                              
                                        })}
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    {data.length>0 && (
                                        <div className='post-footer'>
                                            <Stack>
                                                <Pagination page={page} count={count} size="large" onChange={handleOnChange} />
                                            </Stack>
                                        </div>
                                    )}
                                </Grid>
                            </Grid>
                        </>
                    )}                
            </Container>
        </div>
    )
}


export default Posts;
