import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

const Home =()=>{
    return(
        <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <div className="home-context">
                        <h1> Welcome!</h1> 
                    </div>
                </Grid>                
            </Grid>
        </Box>
        
    )
}
export default Home;