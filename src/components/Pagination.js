import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';

export default function CustomPagination({page, count, handleOnChange}){
    return(
        <Stack spacing={2}>
            <Pagination page={page} count={count} size="large" onChange={handleOnChange} />
        </Stack>
    );
}