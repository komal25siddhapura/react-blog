import { GET_ALL_POST, } from "../application/post.redux/post.types";
import { getPostSuccess, startLoading, stopLoading } from "../application/post.redux/post.actions";
import { getUserSuccess } from "../application/user.redux/user.action";
import { GET_USER } from "../application/user.redux/user.types";
import API from './api';

const PostApiService = (store) => (next) => (action) => {
    
    if(action.type === GET_ALL_POST)
    {
        store.dispatch(startLoading());
        const api=new API("https://dummyjson.com/posts");
        api.makeget().then((response) => {
            //console.log(response.data);
            store.dispatch(getPostSuccess(response?.data?.posts));
            store.dispatch(stopLoading());
        });
        
    }
    if(action.type === GET_USER)
    {        
        store.dispatch(startLoading());
        const userapi=new API("https://dummyjson.com/users/"+action.payload);
        userapi.makeget().then((response) => {
            //console.log(response.data);            
            store.dispatch(getUserSuccess(response?.data));
            store.dispatch(stopLoading());
        });
        
    }

    next(action);
}

export default PostApiService;
