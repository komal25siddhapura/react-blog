import { GET_ALL_POST, GET_POST_BY_ID, GET_ALL_POST_SUCCESS,START_LOADING, STOP_LOADING } from "./post.types"

export const getAllPost = () => {
    return {
        type : GET_ALL_POST
    }
}

export const getPostByID = (id) => {
    return {
        type : GET_POST_BY_ID,
        payload : id
    }
}

export const getPostSuccess = (payload) => {
    return {
        type : GET_ALL_POST_SUCCESS,
        payload 
    }
}


export const startLoading = () => {
    return {
        type : START_LOADING
    }
}

export const stopLoading = () => {
    return {
        type : STOP_LOADING
    }
}
