import { GET_ALL_POST_SUCCESS, START_LOADING, STOP_LOADING } from "./post.types"

const initialState = {
    Post : [],
    IsLoading : false
   
}

const reducer = (state=initialState, action) => {

    switch(action.type)
    {
        case GET_ALL_POST_SUCCESS:
            return {...state, Post:action.payload};    
        case START_LOADING:
            return {...state, IsLoading:true};    
        case STOP_LOADING:
            return {...state, IsLoading:false};         
        default:
            return state;
    }
}

export default reducer;
