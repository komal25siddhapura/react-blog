import { GET_USER_SUCCESS } from "./user.types"


const initialState = {
    user : {}
}

const reducer = (state=initialState, action) => {

    switch(action.type)
    {
        case GET_USER_SUCCESS:
            return {...state, user: action.payload};
        default:
            return state;
    }
}

export default reducer;