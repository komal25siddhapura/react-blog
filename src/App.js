import './App.css';
import Header from './components/Header';
import { Router } from './Routes';
import Navigation from './components/Navigation';
import Box from '@mui/material/Box';

function App() {

  return (
    <Box className='main-body'>
       <Header/>
        <Box className='content-body'>            
          <Navigation Router={Router} />
        </Box>
        {/* <div className='footer-body'>
          <div>
              © 2023 My-Blog All Rights Reserved.
          </div>
        </div> */}
    </Box>
  );
}

export default App;
